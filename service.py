# youtube_dl needs unicode
from __future__ import unicode_literals
# standard lib
import os
import io
import sys
import time
import datetime
import threading
import Queue
import unicodedata
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

# kodi module
import xbmc
import xbmcaddon
import xbmcgui

# hack to make youtube_dl work with xbmc redirected stderr
sys.stderr.isatty = lambda: True

# Global variables
MAX_LIST_LEN = 200
SLEEP_DURATION = 0.25 # pause in main loop
YTDL_RELOAD_PERIOD = 15*60  #reload youtube_dl module every 15 minutes

__addon__       = xbmcaddon.Addon()
__addonname__   = __addon__.getAddonInfo('name')
__icon__        = __addon__.getAddonInfo('icon')

def format_log(message):
    # Add addon name before notification
    message = '[%s] %s' % (__addonname__, message)
    # Remove or convert non ascii characters (xbmc.log doesn't support them)
    message = unicodedata.normalize('NFKD', message)
    message = message.encode('ascii', 'ignore')
    return message

class Logger(object):
    def debug(self, msg):
        xbmc.log(format_log(msg), xbmc.LOGNOTICE)

    def warning(self, msg):
        xbmc.log(format_log(msg), xbmc.LOGNOTICE)

    def error(self, msg):
        xbmc.log(format_log(msg), xbmc.LOGERROR)

def log(message):
    """add message to kodi log"""
    Logger().warning(message)

class Extractor(threading.Thread):
    """Thread extracting information from a media page url
    and putting it in a shared queue"""

    def __init__(self, url, queue):
        self.url = url
        self.queue = queue
        self.init_time = time.time()
        self.monitor = xbmc.Monitor() # monitor xbmc status (exiting)
        self.running = True
        threading.Thread.__init__(self)

    def run(self):
        is_playlist = False # are we dealing with a single media or a playlist page ?
        launched = False # is a media yet launched by this thread ?
        i = 1 # playlist item index
        while self.running and not self.monitor.abortRequested() and i < MAX_LIST_LEN:
            log('New youtube_dl instance for media info extraction')
            ydl_options = {
                # keep going even on strange grounds
                'ignoreerrors': True,
                # more details
                'verbose': True,
                # choose best format with both audio and video
                'format': 'best',
                # log to xbmc log
                'logger': Logger(),
                # get only one item from playlist
                'playlist_items': str(i),
                }

            with youtube_dl.YoutubeDL(ydl_options) as ydl:
                try:
                    info = ydl.extract_info(self.url, download=False)
                except youtube_dl.utils.DownloadError:
                    log('Youtube_dl error while extracting from '+self.url)
                    info = None

            # Detect playlist
            if info != None and '_type' in info and info['_type'] == 'playlist':
                is_playlist = True
                if len(info['entries']) > 0:
                    # Extract item
                    info = info['entries'][0]
                else:
                    log('End of playlist reached at '+self.url)
                    break

            # Process item
            if info != None and 'url' in info:
                stream_url = info['url']
                name = info['title']+' - '+info['id']
                if not launched:
                    self.queue.put(('play', name, self.url, stream_url, self.init_time))
                    # Stop older extractions
                    for extraction in EXTLIST:
                        if extraction.init_time < self.init_time:
                            extraction.stop()
                            EXTLIST.remove(extraction)
                    EXTLIST.append(self)
                    # Exit if treating single media
                    if not is_playlist:
                        break
                    launched = True
                else:
                    self.queue.put(('add', name, self.url, stream_url, self.init_time))

            # Exit if failed extraction on single item
            if info == None and not is_playlist:
                break

            i += 1
        else:
            log('Extraction aborted before end for '+self.url)

    def stop(self):
        log('Stopping extraction from '+self.url)
        self.running = False

class Handler(BaseHTTPRequestHandler):
    '''simple handler reacting to requests'''
    def do_GET(self):
        # if receiving watch instruction
        if self.path.startswith('/watch?url='):
            # SHOULD SANITIZE URL !!!
            url = self.path[11:]

            dialog = xbmcgui.Dialog()
            dialog.notification('Casting', url, xbmcgui.NOTIFICATION_INFO, 5000)

            # send http response
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write("""<html>
                <head><title>""" + url + """</title></head>
                <body>Trying to play """ + url + """ ...</body>
                </html>""")

            # send request to media info extractor
            ext = Extractor(url, MEDIAQUEUE)
            ext.start()
        else:
            self.send_error(404,'File Not Found: %s' % self.path)

class Server(threading.Thread):
    """Thread for http server handling new media requests"""
    def __init__(self):
        # Interface listening for web playing events
        self.host = ''
        self.port = int(__addon__.getSetting('port'))
        self.server = HTTPServer((self.host, self.port), Handler)
        threading.Thread.__init__(self)

    def run(self):
        log( "Starting http server for web media requests on port " + str(self.port))
        self.server.serve_forever()

    def stop(self):
        log('Shutting down http server')
        self.server.shutdown()

def write_log(name, url):
    '''add watched video info in log file'''
    log_path = __addon__.getSetting('logfile')
    if log_path != "":
        with io.open(log_path,'a', encoding='utf-8') as info_log:
            time = str(datetime.datetime.now())
            info_log.write(time+'\t'+name+'\t'+url+'\n')
            log('Keeping media information in '+log_path)

if __name__ == '__main__':
    try:
        # native youtube dl
        import youtube_dl
    except ImportError:
        log('Missing youtube_dl in python path (sudo pip install youtube_dl)')
    else:
        log('Module youtube_dl imported with success')
        # global queue for multithreading
        MEDIAQUEUE = Queue.Queue()
        # extractors list
        EXTLIST = []
        # monitor xbmc status (exiting)
        monitor = xbmc.Monitor()
        # the player and playlist to control
        web_player = xbmc.Player()
        video_playlist = xbmc.PlayList(xbmc.PLAYLIST_VIDEO)
        # http server to watch for requests
        httpd = Server()
        httpd.start()
        # main loop
        flip = time.time()
        last_request = 0
        while not monitor.abortRequested():
            # watch for requests and launch player
            if not MEDIAQUEUE.empty():
                mode, name, url, stream_url, t = MEDIAQUEUE.get()
                #are we dealing with a single item ?
                if mode == 'play':
                    log('Play mode')
                    # accept only last request
                    if t > last_request:
                        last_request = t
                        # clear playlist
                        video_playlist.clear()
                        # add entry to Kodi playlist
                        listitem = xbmcgui.ListItem()
                        listitem.setLabel(name)
                        video_playlist.add(stream_url, listitem)
                        # lets play
                        web_player.play(video_playlist)
                        # keep trace of watching activty
                        write_log(name, url)
                # or a playlist item to add ? BEWARE we do not log these
                elif mode == 'add':
                    log('Add mode')
                    # complete only currently active playlist
                    if t == last_request:
                        # add entry to Kodi playlist
                        listitem = xbmcgui.ListItem()
                        listitem.setLabel(name)
                        video_playlist.add(stream_url, listitem)
            # reload ytdl module as it may have been updated by cron
            if time.time() > flip + YTDL_RELOAD_PERIOD:
                flip = time.time()
                youtube_dl = reload(youtube_dl)
            # sleep to preserve cpu
            time.sleep(SLEEP_DURATION)
        else:
            httpd.stop()
            log('Stopping service')
